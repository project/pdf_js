<?php

namespace Drupal\pdf_js\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'link_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "pdf_js_formatter",
 *   label = @Translation("PDF.js Viewer"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class PDFJSFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
      'download' => true,
      'fullscreen' => true,
      'zoom' => true,
      'label' => t('View'),
      'classes' => '',
      'attributes' => ''
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      'download' => [
        '#type' => 'checkbox',
        '#title' => t('Download button'),
        '#default_value' => $this->getSetting('download'),
      ],
      'fullscreen' => [
        '#type' => 'checkbox',
        '#title' => t('Fullscreen button'),
        '#default_value' => $this->getSetting('fullscreen'),
      ],
      'zoom' => [
        '#type' => 'checkbox',
        '#title' => t('Zoom In / Out button'),
        '#default_value' => $this->getSetting('zoom'),
      ],
      'label' => [
        '#type' => 'textfield',
        '#title' => t('Link label'),
        '#default_value' => $this->getSetting('label'),
      ],
      'classes' => [
        '#type' => 'textfield',
        '#title' => t('Add additional classes for the link'),
        '#description' => t('Add space between each class'),
        '#default_value' => $this->getSetting('classes'),
      ],
      'attributes' => [
        '#type' => 'textfield',
        '#title' => t('Add additional attributes to the link'),
        '#description' => t('Add space between each attribute eg. data-attribute=test data-attribute=test2'),
        '#default_value' => $this->getSetting('attributes'),
      ],

    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $summary[] = t('Download button: @download_button', array('@download_button' => $this->getSetting('download')));
    $summary[] = t('Fullscreen button: @fullscreen_button', array('@fullscreen_button' => $this->getSetting('fullscreen')));
    $summary[] = t('Zoom In / Out button: @zoom_in_out_button', array('@zoom_in_out_button' => $this->getSetting('zoom')));
    $summary[] = t('Label: @label', array('@label' => $this->getSetting('label')));
    $summary[] = t('Classes: @classes', array('@classes' => $this->getSetting('classes')));
    $summary[] = t('Attributes: @attributes', array('@attributes' => $this->getSetting('attributes')));
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $fid = $item->getValue();
      $elements[$delta] = [
        '#theme' => 'pdf_js_formatter',
        '#item' => [
          'fid' => $fid,
          'download' => $this->getSetting('download'),
          'fullscreen' => $this->getSetting('fullscreen'),
          'zoom' => $this->getSetting('zoom'),
          'label'=> $this->getSetting('label'),
          'classes'=> $this->getSetting('classes'),
          'attributes'=> $this->getSetting('attributes')
        ],
        '#context' => [
          'value' => $item->value
        ],
        '#attached' => [
          'library' => ['pdf_js/pdf-js']
        ]
      ];
    }

    return $elements;
  }
}
