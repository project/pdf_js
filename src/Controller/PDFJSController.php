<?php
/**
 * @file
 * Contains \Drupal\pdf_js\Controller\PDFViewerController.
 */

namespace Drupal\pdf_js\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PDFJSController extends ControllerBase {

  /**
   * Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * ManagePaymentDetailsController constructor.
   *
   * @param $entityTypeManager
   */
  public function __construct($entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * PDFJS Viewer
   *
   *
   * @param $fid
   * @param $download
   * @param $fullscreen
   * @param $zoom
   *
   * @return array
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildPDFViewer($fid, $download, $fullscreen, $zoom){
    $build = [];
    /** @var \Drupal\file\Entity\File $file */
    $file = $this->entityTypeManager->getStorage('file')->load($fid);
    if($file instanceof $file && !file_validate_extensions($file,'pdf')) {
      $url = $file->getFileUri();
      $build = [
        '#item' => [
          'fid' => $fid,
          'download' => $download,
          'fullscreen' => $fullscreen,
          'zoom' => $zoom,
        ],
        '#theme' => 'pdf_js_controller',
        '#attached' => [
          'drupalSettings' => [
            'pdfJs' =>[
              'fileUrl' => file_create_url($url),
              'download' => $download,
              'fullscreenButton' => $fullscreen,
              'zoom'=> $zoom
            ]
          ],
          'library' => [
            'pdf_js/pdf-js'
          ]
        ]
      ];
    }
    return $build;
  }
}

