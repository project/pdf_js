(($, Drupal) => {
  Drupal.behaviors.pdfJsBehaviour = {
    attach(context, settings) {
      function populateModal() {
        const viewer = $("#drupal-modal");
        // check if tag for pdf viewer exists and then intialize viewer
        if ($("#pdf-wrapper-1").length && !viewer.hasClass("pdf-initialized")) {
          Drupal.behaviors.pdfJsBehaviour.initializePDFViewer(
            context,
            settings
          );
          viewer.addClass("pdf-initialized");
        }
      }
      // pdf js initialize
      // support for bootstrap
      if (Drupal.hasOwnProperty("bootstrap")) {
        $("#drupal-modal").on("shown.bs.modal", () => {
          populateModal();
        });
      } else {
        // if there is no bootstrap then add fallback to drupal dialog
        $("#drupal-modal").on("openDialog", () => {
          populateModal();
        });
      }
    },
    initializePDFViewer(context, settings) {
      // If absolute URL from the remote server is provided, configure the CORS
      // header on that server.
      /**
       * @param {{pdfJs:object}} settings
       * @param {{fileUrl:string}} settings.pdfJs
       */
      const url = settings.pdfJs.fileUrl;

      // Loaded via <script> tag, create shortcut to access PDF.js exports.
      /**
       * @param {{GlobalWorkerOptions:object}} pdfjsLib
       */
      const pdfjsLib = window["pdfjs-dist/build/pdf"];

      // The workerSrc property shall be specified.
      pdfjsLib.GlobalWorkerOptions.workerSrc =
        "//mozilla.github.io/pdf.js/build/pdf.worker.js";

      let pdfDoc = null;
      // let pageRendering = false;
      let pageNumPending = null;
      let scale = 1;
      let canvas = document.getElementById("pdf-wrapper-1");
      let ctx = canvas.getContext("2d");

      /**
       * Get page info from document, resize canvas accordingly, and render
       * page.
       * @param {number} num Page number.
       */
      function renderPage(num) {
        // Using promise to fetch the page
        /**
         * @param {{getPage:Function}} pdfDoc
         */
        pdfDoc.getPage(num).then(page => {
          const canvasId = `pdf-wrapper-${num}`;
          if (!$(`#pdf-wrapper-${num}`).length) {
            $(".pdf-wrapper").append($("<canvas/>", { id: canvasId }));
          }
          canvas = document.getElementById(canvasId);
          ctx = canvas.getContext("2d");
          /**
           * @param {{getViewport:Function}} page
           */
          const viewport = page.getViewport({
            scale
          });
          canvas.height = viewport.height;
          canvas.width = viewport.width;

          // Render PDF page into canvas context
          const renderContext = {
            canvasContext: ctx,
            viewport
          };
          const renderTask = page.render(renderContext);

          // Wait for rendering to finish
          renderTask.promise.then(() => {
            // let pageRendering = false;
            if (pageNumPending !== null) {
              // New page rendering is pending
              renderPage(pageNumPending);
              pageNumPending = null;
            }
          });
        });

        document.getElementById("download").href = url;
      }

      /**
       * Render all pages
       */
      function renderAllPages() {
        for (let i = 1; i <= pdfDoc.numPages; i++) {
          renderPage(i);
        }
      }

      /**
       * Initialize zoom buttons if selected
       */
      function initializeZoom() {
        $("#zoomin").click(() => {
          scale += 0.25;
          renderAllPages();
        });
        $("#zoomout").click(() => {
          if (scale <= 0.25) {
            return;
          }
          scale -= 0.25;
          renderAllPages();
        });
      }

      // if zoom selected then initialize
      if (settings.pdfJs.zoom) {
        initializeZoom();
      }

      // if fullscreen button selected
      /**
       * @param {{pdfJs:object}} settings
       * @param {{fullscreenButton:number}} settings.pdfJs
       */
      if (settings.pdfJs.fullscreenButton) {
        $("#fullscreen").on("click", event => {
          const pdfWrapper = $(".pdf-wrapper");
          event.preventDefault();
          if (pdfWrapper.hasClass("sized")) {
            pdfWrapper.removeClass("sized");
            scale = 1;
          } else {
            pdfWrapper.addClass("sized");
            scale = 2;
          }
          renderAllPages();
        });
      }

      // change page number on scroll
      $(document).on("scroll", () => {
        for (let i = 1; i <= pdfDoc.numPages; i++) {
          if ($(this).scrollTop() >= $(`#pdf-wrapper-${i}`).position().top) {
            document.getElementById("page_num").textContent = i.toString();
          }
        }
      });

      /**
       * Error handler
       *
       * @param {string} reason Reason error occurred
       *
       * @return {string} reason
       */
      function errorHandler(reason) {
        // @todo we need to change this according to how we plan on handling errors
        return reason;
      }

      /**
       * Asynchronously downloads PDF.
       */
      pdfjsLib.getDocument(url).promise.then(
        pdfDoc_ => {
          pdfDoc = pdfDoc_;
          renderAllPages();
          document.getElementById("page_count").textContent = pdfDoc.numPages;
        },
        reason => {
          errorHandler(reason);
        }
      );
    }
  };
})(jQuery, Drupal);
